#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
	// Given
	    int value[] = {-8};
	    // When
	    int amount = valuesNumber(-4, value, 1);
	    // Then
	    assertEquals_int(0, amount);
	}


void testExercise_B() {
	// Given
		    int value[] = {-1};
		    // When
		    int amount = valuesNumber(-1, value, 1);
		    // Then
		    assertEquals_int(1, amount);
		}



void testExercise_C() {
	// Given
			    int value[] = {22, 22};
			    // When
			    int amount = valuesNumber(22, value, 2);
			    // Then
			    assertEquals_int(2, amount);
			}


void testExercise_D() {
	// Given
		    int value[] = {67, 67, -4, 67, 67, 22};
		    // When
		    int amount = valuesNumber(67, value, 6);
		    // Then
		    assertEquals_int(4, amount);
		}

void testExercise_E() {
	// Given
		    int value[] = {67, 25, -8, 25, 22, -4, 22, -1, 25};
		    // When
		    int amount = valuesNumber(25, value, 9) ;
		    // Then
		    assertEquals_int(3, amount);
		}
