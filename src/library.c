#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int valuesNumber(int foundValue, int* value, int valueLength) {
    
    int valuesNumberResult = 0;
    
    for (int k=0; k<valueLength ; k++) {
            if(value[k] == foundValue) {
                valuesNumberResult++;
            }
    }
    
    return valuesNumberResult;
}

